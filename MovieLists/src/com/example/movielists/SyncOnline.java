package com.example.movielists;

import java.io.DataOutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.apache.http.HttpResponse;
import org.apache.http.client.ResponseHandler;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.ContentValues;
import android.content.res.Resources.NotFoundException;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;

public class SyncOnline extends Activity implements OnClickListener{
	//check if user
	//get last sync time
	//push pull since last time
	private Button syncNow,logout;
	private TextView lastSync;
	private EditText userName,password;
	private Switch dailySync;
	private SQLiteDatabase db;
	private String dateNow="";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.sync_screen);
		initialize();
	}
	private void initialize(){
		syncNow=(Button)findViewById(R.id.bsyncnow);
		logout=(Button)findViewById(R.id.blogout);
		lastSync=(TextView)findViewById(R.id.tlastsync);
		userName=(EditText)findViewById(R.id.tUser);
		password=(EditText)findViewById(R.id.tPassword);
		dailySync=(Switch)findViewById(R.id.swdaily);
		syncNow.setOnClickListener(this);
		logout.setOnClickListener(this);

		db = MovieDbHelper.getInstance(this).getWritableDatabase();
		db.execSQL(MovieDbContract.UserDb.SQL_CREATE_ENTRIES);
		Cursor cursor=db.query(MovieDbContract.UserDb.TABLE_NAME,
				null, null, null, null, null, null);
		if(cursor!=null&&cursor.getCount()>0)
		{
			cursor.moveToFirst();
			String user = cursor.getString(cursor
					.getColumnIndex(MovieDbContract.UserDb.C_USER));
			String passworddb = cursor.getString(cursor
					.getColumnIndex(MovieDbContract.UserDb.C_PASSWORD));
			String lastSyncTime = cursor.getString(cursor
					.getColumnIndex(MovieDbContract.UserDb.C_SYNCTIME));
			userName.setText(user);
			password.setText(passworddb);
			lastSync.setText(lastSyncTime);
			userName.setEnabled(false);
			password.setEnabled(false);
			cursor.close();
		}
		else{
			if(userName.requestFocus())
				getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
		}

	}

	@Override
	public void onClick(View button) {
		switch (button.getId()) {
		case(R.id.blogout):
			userName.setText("");
		password.setText("");
		lastSync.setText("");
		userName.setEnabled(true);
		password.setEnabled(true);
		//		db.execSQL(MovieDbContract.MovieDb.SQL_DELETE_ENTRIES);
		db.execSQL(MovieDbContract.UserDb.SQL_DELETE_ENTRIES);
		break;
		case(R.id.bsyncnow):
			JSONObject jsonOutput=new JSONObject();
		//TODO--move below...datenow
		//post update
		Cursor vals=db.query(MovieDbContract.MovieDb.TABLE_NAME, MovieDbContract.MovieDb.ALL_COLUMNS, 
				null, null, null, null, null);
		if(null!=vals && vals.getCount()>0){
			vals.moveToFirst();
			JSONObject jFragment=new JSONObject();
			try {
				jFragment.put("user",userName.getText().toString());
				jFragment.put("password",password.getText().toString());
				jsonOutput.put("login",jFragment);
				do{
					jFragment=new JSONObject();
					Log.i("TAG",vals.getString(vals
							.getColumnIndex(MovieDbContract.MovieDb.C_NAME)));
					jFragment.put("name",vals.getString(vals
							.getColumnIndex(MovieDbContract.MovieDb.C_NAME)));
					jFragment.put("year",vals.getString(vals
							.getColumnIndex(MovieDbContract.MovieDb.C_YEAR)));
					jFragment.put("imdb",vals.getString(vals
							.getColumnIndex(MovieDbContract.MovieDb.C_IMDB)));
					jFragment.put("meta",vals.getString(vals
							.getColumnIndex(MovieDbContract.MovieDb.C_META)));
					jFragment.put("rating",vals.getString(vals
							.getColumnIndex(MovieDbContract.MovieDb.C_RATING)));
					jFragment.put("link",vals.getString(vals
							.getColumnIndex(MovieDbContract.MovieDb.C_ID)));
					jsonOutput.accumulate("movie",jFragment);
					vals.moveToNext();
				}while(!vals.isAfterLast());
				lastSync.setText(dateNow);
			}catch (JSONException e) {
				e.printStackTrace();
			}
		}
		vals.close();
		//post json to server
		try {
			System.out.println(jsonOutput.toString());
			//httpPost(getResources().getString(R.string.onlinepath), jsonOutput.toString());
			if(userName.isEnabled()){
				dateNow=SimpleDateFormat.getDateInstance(DateFormat.MEDIUM).format(Calendar.getInstance().getTime());
				userName.setEnabled(false);
				password.setEnabled(false);
				ContentValues value=new ContentValues();
				value.put(MovieDbContract.UserDb.C_USER, userName.getText().toString());
				value.put(MovieDbContract.UserDb.C_PASSWORD, password.getText().toString());
				value.put(MovieDbContract.UserDb.C_SYNCTIME, dateNow);
				db.insert(MovieDbContract.UserDb.TABLE_NAME, null, value);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		break;
		}

	}
	@Override
	protected void onPause() {
		super.onPause();
		if(null!=db&&db.isOpen())
			db.close();
	}

	private int httpPost(String path, String se) throws Exception{
		URL url=new URL(path);
		HttpURLConnection con=(HttpURLConnection)url.openConnection();
		con.setRequestMethod("POST");
		con.setRequestProperty("Content-Type", "application/json; charset=utf8");
		con.setDoOutput(true);
		DataOutputStream wr = new DataOutputStream(con.getOutputStream());
		wr.writeBytes(se);
		wr.flush();
		wr.close();
		int responseCode = con.getResponseCode();
		System.out.println("\nSending 'POST' request to URL : " + url);
		System.out.println("Post parameters : " + se);
		System.out.println("Response Code : " + responseCode);
		return responseCode;
	}

}
