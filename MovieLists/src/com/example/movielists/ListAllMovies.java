package com.example.movielists;

import android.app.ListActivity;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

public class ListAllMovies extends ListActivity {

	SQLiteDatabase db;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.listmovies);

		db=MovieDbHelper.getInstance(this).getReadableDatabase();
		Cursor cursor=db.query(MovieDbContract.MovieDb.TABLE_NAME,
				MovieDbContract.MovieDb.ALL_COLUMNS, null, null, null, null, null);
		setListAdapter(new SimpleCursorAdapter(this, R.layout.listrow, cursor, MovieDbContract.MovieDb.ALL_COLUMNS
				, MovieDbContract.MovieDb.ALL_COLUMNS_REF, 0));
	}
	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);
	}
//TODO-add buttons and alternate colors 

}
