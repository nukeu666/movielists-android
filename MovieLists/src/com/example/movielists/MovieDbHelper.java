package com.example.movielists;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class MovieDbHelper extends SQLiteOpenHelper {

	private static MovieDbHelper sInstance = null;

	public static MovieDbHelper getInstance(Context context) {
		if (null == sInstance){
			sInstance = new MovieDbHelper(context.getApplicationContext());
		}
		return sInstance;
	}

	private MovieDbHelper(Context context) {
		super(context, MovieDbContract.DATABASE_NAME, null,
				MovieDbContract.DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(MovieDbContract.MovieDb.SQL_CREATE_ENTRIES);
		db.execSQL(MovieDbContract.UserDb.SQL_CREATE_ENTRIES);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int arg1, int arg2) {
		db.execSQL(MovieDbContract.MovieDb.SQL_DELETE_ENTRIES);
		db.execSQL(MovieDbContract.UserDb.SQL_DELETE_ENTRIES);
		onCreate(db);
	}

}
