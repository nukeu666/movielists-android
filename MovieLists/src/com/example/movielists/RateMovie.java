package com.example.movielists;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;

public class RateMovie extends Activity implements OnClickListener {

	static final String TAG = "RateMovie";
	private final static String TAG_TITLE = "Title";
	private final static String TAG_YEAR = "Year";
	private final static String TAG_IMDB = "imdbRating";
	private final static String TAG_META = "Metascore";
	private final static String TAG_URLID = "imdbID";
	MovieDbHelper dbHelper;

	EditText movieName;
	Button findMovie, submit;
	RatingBar rb;
	TextView name, year, imdb, meta, urlId;
	ProgressBar pbbar;
	SQLiteDatabase db;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.rate_new);
		initialize();
	}

	private void initialize() {
		movieName = (EditText) findViewById(R.id.etmoviename);
		findMovie = (Button) findViewById(R.id.bfindmovie);
		submit = (Button) findViewById(R.id.bsubmit);
		rb = (RatingBar) findViewById(R.id.rbbar);
		name = (TextView) findViewById(R.id.tvname);
		year = (TextView) findViewById(R.id.tvyear);
		imdb = (TextView) findViewById(R.id.tvimdb);
		meta = (TextView) findViewById(R.id.tvmeta);
		urlId = (TextView) findViewById(R.id.tvlink);
		pbbar = (ProgressBar) findViewById(R.id.pbbar);
		findMovie.setOnClickListener(this);
		submit.setOnClickListener(this);
		submit.setEnabled(false);
		db = MovieDbHelper.getInstance(this).getWritableDatabase();
	}

	@Override
	public void onClick(View button) {
		switch (button.getId()) {
		case (R.id.bsubmit):

			ContentValues vals=new ContentValues();
		vals.put(MovieDbContract.MovieDb.C_NAME, name.getText().toString().substring(this.getString(R.string.name).length()));
		vals.put(MovieDbContract.MovieDb.C_YEAR, year.getText().toString().substring(this.getString(R.string.year).length()));
		vals.put(MovieDbContract.MovieDb.C_IMDB, imdb.getText().toString().substring(this.getString(R.string.imdb).length()));
		vals.put(MovieDbContract.MovieDb.C_META, meta.getText().toString().substring(this.getString(R.string.meta).length()));
		vals.put(MovieDbContract.MovieDb.C_RATING, rb.getRating() * 2);
		vals.put(MovieDbContract.MovieDb.C_ID, urlId.getText().toString());
		db.insert(MovieDbContract.MovieDb.TABLE_NAME, null, vals);
		finish();
		break;

		case (R.id.bfindmovie):
			String getMovieName = movieName.getText().toString();
		if (null == getMovieName || getMovieName.length() == 0)
			return;
		new getDetails().execute();
		break;
		}

	}

	private class getDetails extends AsyncTask<Void, Void, Void> {
		String jName,jYear,jIMDB,jMeta,jurlId;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			name.setVisibility(View.GONE);
			year.setVisibility(View.GONE);
			imdb.setVisibility(View.GONE);
			meta.setVisibility(View.GONE);
			urlId.setVisibility(View.GONE);
			pbbar.setVisibility(View.VISIBLE);
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);

			if (null == jName || jName.equalsIgnoreCase("null")) {
				name.setText("Movie not found");
				name.setVisibility(View.VISIBLE);
				pbbar.setVisibility(View.GONE);
			} else {
				name.setText(getApplicationContext().getString(R.string.name) + jName);
				year.setText(getApplicationContext().getString(R.string.year) + jYear);
				imdb.setText(getApplicationContext().getString(R.string.imdb) + jIMDB);
				meta.setText(getApplicationContext().getString(R.string.meta) + jMeta);
				urlId.setText("http://imdb.com/title/"+jurlId+"/");
				urlId.setMovementMethod(LinkMovementMethod.getInstance());
				name.setVisibility(View.VISIBLE);
				year.setVisibility(View.VISIBLE);
				imdb.setVisibility(View.VISIBLE);
				meta.setVisibility(View.VISIBLE);
				urlId.setVisibility(View.VISIBLE);
				pbbar.setVisibility(View.GONE);
				submit.setEnabled(true);
			}
		}

		@Override
		protected Void doInBackground(Void... arg0) {
			QueryService q = new QueryService();
			String movie = movieName.getText().toString();
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair("t", movie));
			String data = q.makeCall("http://www.omdbapi.com/", params);

			Log.d(TAG, "DATA:" + data);
			if (null != data) {
				try {
					JSONObject jObj = new JSONObject(data);
					jName = jObj.getString(TAG_TITLE);
					jYear = jObj.getString(TAG_YEAR);
					jIMDB = jObj.getString(TAG_IMDB);
					jMeta = jObj.getString(TAG_META);
					jurlId=jObj.getString(TAG_URLID);
				} catch (JSONException e) {
					Log.e(TAG, e.getMessage());
				}
			}
			return null;
		}
	}

	@Override
	protected void onPause() {
		super.onPause();
		if(db!=null&&db.isOpen())db.close();
	}
}
