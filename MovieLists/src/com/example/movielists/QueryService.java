package com.example.movielists;

import java.io.IOException;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

public class QueryService {
	public String makeCall(String url, List<NameValuePair> params) {
		DefaultHttpClient client = new DefaultHttpClient();
		String data = null;
		HttpResponse res = null;
		if (null != params) {
			String paramString = URLEncodedUtils.format(params, "UTF-8");
			url += "?"+paramString;
		}
		System.out.println(url);
		HttpGet get = new HttpGet(url);
		try {
			res = client.execute(get);
			data = EntityUtils.toString(res.getEntity());
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return data;
	}
}
