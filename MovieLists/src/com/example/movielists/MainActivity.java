package com.example.movielists;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends Activity implements OnClickListener {

	Button newMovies, listAll, getStats, clearAll, syncOnline;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		initialize();
	}

	private void initialize() {
		newMovies = (Button) findViewById(R.id.bratenew);
		listAll = (Button) findViewById(R.id.blistall);
		getStats = (Button) findViewById(R.id.bcheckstats);
		clearAll = (Button) findViewById(R.id.bclearall);
		syncOnline = (Button) findViewById(R.id.bsync);
		newMovies.setOnClickListener(this);
		listAll.setOnClickListener(this);
		getStats.setOnClickListener(this);
		clearAll.setOnClickListener(this);
		syncOnline.setOnClickListener(this);
	}

	@Override
	public void onClick(View button) {
		Intent i;
		switch (button.getId()) {
		case (R.id.bratenew):
			i = new Intent(this, RateMovie.class);
		startActivity(i);
		break;
		case (R.id.blistall):
			i = new Intent(this, ListAllMovies.class);
		startActivity(i);
		break;
		case (R.id.bcheckstats):
			i=new StatChart().getIntent(this);
		startActivity(i);
		break;
		case (R.id.bsync):
			i=new Intent(this, SyncOnline.class);
		startActivity(i);
		break;
		case (R.id.bclearall):
			final Runnable del = new Runnable() {
			@Override
			public void run() {
				getApplicationContext().deleteDatabase(
						MovieDbContract.DATABASE_NAME);
				Toast.makeText(getApplicationContext(), R.string.clearlist,
						Toast.LENGTH_SHORT).show();
			}};
			ConfirmationDialog.confirm(this, del);

			break;
		}
	}

}
