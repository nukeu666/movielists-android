package com.example.movielists;

import org.achartengine.ChartFactory;
import org.achartengine.chart.LineChart;
import org.achartengine.chart.PointStyle;
import org.achartengine.chart.ScatterChart;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.model.XYSeries;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;

public class StatChart {
	private XYMultipleSeriesDataset mDataset = new XYMultipleSeriesDataset();
	private XYMultipleSeriesRenderer mRenderer = new XYMultipleSeriesRenderer();
	private XYSeries mCurrentSeries;
	private XYSeriesRenderer mCurrentRenderer;
	private SQLiteDatabase db;

	public Intent getIntent(Context context) {
		db = MovieDbHelper.getInstance(context).getReadableDatabase();
		//		Cursor cursor = db.query(MovieDbContract.MovieDb.TABLE_NAME,
		//				MovieDbContract.MovieDb.ALL_COLUMNS, null, null, null, null,
		//				null);
		Cursor cursor=db.rawQuery("select * from "+MovieDbContract.MovieDb.TABLE_NAME, null);
		mCurrentSeries=new XYSeries("title");
		mCurrentRenderer=new XYSeriesRenderer();
		if(cursor!=null)
			cursor.moveToFirst();
		do{
			float imdb = cursor.getFloat(cursor
					.getColumnIndex(MovieDbContract.MovieDb.C_IMDB));
			float rating = cursor.getFloat(cursor
					.getColumnIndex(MovieDbContract.MovieDb.C_RATING));
//			System.out.println(imdb + " " + rating);
			mCurrentSeries.add(imdb, rating);
		}while(cursor.moveToNext());
		mDataset.addSeries(mCurrentSeries);
		mCurrentRenderer.setColor(Color.BLACK);
		mCurrentRenderer.setPointStyle(PointStyle.CIRCLE);
		mCurrentRenderer.setFillPoints(true);
		mRenderer.addSeriesRenderer(mCurrentRenderer);
		
		//Axis
		mCurrentSeries=new XYSeries("");
		mCurrentRenderer=new XYSeriesRenderer();
		mCurrentSeries.add(0, 0);
		mCurrentSeries.add(10, 10);
		mCurrentRenderer.setColor(Color.BLUE);
		
		mDataset.addSeries(mCurrentSeries);
		mRenderer.addSeriesRenderer(mCurrentRenderer);
		
		mRenderer.setXAxisMin(0);
		mRenderer.setXAxisMax(10.0);
		mRenderer.setYAxisMin(0);
		mRenderer.setYAxisMax(10.0);
		
		if(db!=null&&db.isOpen())db.close();

		return ChartFactory.getCombinedXYChartIntent(context, mDataset, mRenderer, 
				new String[] { ScatterChart.TYPE, LineChart.TYPE,}, "Variance");
	}
}
