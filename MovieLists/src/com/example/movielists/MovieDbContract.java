package com.example.movielists;

import android.provider.BaseColumns;

public class MovieDbContract {
	public MovieDbContract(){}
	public static final int DATABASE_VERSION = 1;
	public static final String DATABASE_NAME = "movieapp.db";
	public static final String TEXT_TYPE = " TEXT";
	public static final String COMMA_SEP = ",";
	public static abstract class MovieDb implements BaseColumns{
		public static final String TABLE_NAME="moviedb";	
		public static final String C_NAME="name";
		public static final String C_YEAR="year";
		public static final String C_IMDB="imdb";
		public static final String C_META="meta";
		public static final String C_RATING="rating";
		public static final String C_ID="id";
	
		public static final String SQL_CREATE_ENTRIES =
				"CREATE TABLE IF NOT EXISTS " + MovieDb.TABLE_NAME + " (" +
						MovieDb._ID + " INTEGER PRIMARY KEY," +
						MovieDb.C_NAME + TEXT_TYPE + COMMA_SEP +
						MovieDb.C_YEAR + TEXT_TYPE + COMMA_SEP +
						MovieDb.C_IMDB + TEXT_TYPE + COMMA_SEP +
						MovieDb.C_META + TEXT_TYPE + COMMA_SEP +
						MovieDb.C_RATING + TEXT_TYPE + COMMA_SEP +
						MovieDb.C_ID + TEXT_TYPE + " )";

		public static final String SQL_DELETE_ENTRIES =
				"DROP TABLE IF EXISTS " + MovieDb.TABLE_NAME;
		
		public static final String[] ALL_COLUMNS={"_id",C_NAME,C_YEAR,C_IMDB,C_META,C_RATING,C_ID};
		public static final int[] ALL_COLUMNS_REF={0,R.id.tname,R.id.tyear,R.id.timdb,R.id.tmeta,R.id.trating,R.id.tvlink};
	}
	public static abstract class UserDb implements BaseColumns{
		public static final String TABLE_NAME="userdb";
		
		public static final String C_USER="name";
		public static final String C_SYNCTIME="synctime";
		public static final String C_PASSWORD="password";
		
		public static final String SQL_CREATE_ENTRIES =
				"CREATE TABLE IF NOT EXISTS " + UserDb.TABLE_NAME + " (" +
						UserDb._ID + " INTEGER PRIMARY KEY," +
						UserDb.C_USER + TEXT_TYPE + COMMA_SEP +
						UserDb.C_SYNCTIME+ TEXT_TYPE + COMMA_SEP +
						UserDb.C_PASSWORD + TEXT_TYPE + " )";

		public static final String SQL_DELETE_ENTRIES =
				"DROP TABLE IF EXISTS " + UserDb.TABLE_NAME;
		
		public static final String[] ALL_COLUMNS={"_id", C_USER, C_SYNCTIME, C_PASSWORD};
	}
}
